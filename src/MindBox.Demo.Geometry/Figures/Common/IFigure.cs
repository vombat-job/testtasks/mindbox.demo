namespace MindBox.Demo.Geometry.Figures.Common
{
    public interface IFigure
    {
        double GetSquare();
    }
}