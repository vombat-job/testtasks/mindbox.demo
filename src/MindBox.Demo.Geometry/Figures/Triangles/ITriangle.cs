using MindBox.Demo.Geometry.Figures.Common;

namespace MindBox.Demo.Geometry.Figures.Triangles
{
    public interface ITriangle : IFigure
    {
        double SideA { get; }
        double SideB { get; }
        double SideC { get; }

        bool IsRightTriangle { get; }
    }
}