namespace MindBox.Demo.Geometry.Figures.Triangles
{
    public class Triangle : ITriangle
    {
        public double SideA { get; }
        public double SideB { get; }
        public double SideC { get; }
        public bool IsRightTriangle { get; } 

        public Triangle(double sideA, double sideB, double sideC)
        {
            ValidateSide(sideA);
            ValidateSide(sideB);
            ValidateSide(sideC);

            ValidateSumSides(sideA, sideB, sideC);

            SideA = sideA;
            SideB = sideB;
            SideC = sideC;

            IsRightTriangle = CheckIsRightTriangle();
        }
        
        private bool CheckIsRightTriangle()
        {
            double[] sides = { SideA, SideB, SideC };
            double maxSide = sides.Max();
            var sumLegs = sides.Where(s => s != maxSide).Aggregate((x, y) => Math.Pow(x, 2) + Math.Pow(x, 2));

            return Math.Abs(Math.Pow(maxSide, 2) - sumLegs) < Constants.CalculationAccuracy;
        }

        public double GetSquare()
        {
            var halfP = (SideA + SideB + SideC) / 2d;
            var square = Math.Sqrt(halfP * (halfP - SideA) * (halfP - SideB) * (halfP - SideC));
            return square;
        }

        private static void ValidateSide(double side)
        {
            if (side < Constants.CalculationAccuracy)
                throw new ArgumentException("Неверно указана сторона.", nameof(side));
        }
        private static void ValidateSumSides(double sideA, double sideB, double sideC)
        {
            var maxSide = Math.Max(sideA, Math.Max(sideB, sideC));
            var perimeter = sideA + sideB + sideC;

            if (perimeter - maxSide - maxSide < Constants.CalculationAccuracy)
                throw new ArgumentException("Наибольшая сторона треугольника должна быть меньше суммы других сторон");
        }
    }
}