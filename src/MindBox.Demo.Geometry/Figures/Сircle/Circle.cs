﻿using MindBox.Demo.Geometry.Figures.Common;

namespace MindBox.Demo.Geometry.Figures.Сircle
{
    public class Circle : IFigure
    {
        public double radius { get; }

        public Circle(double radius)
        {
            ValidateRadius(radius);

            this.radius = radius;
        }

        public double GetSquare() => Math.PI * Math.Pow(radius, 2d);

        private void ValidateRadius(double radius)
        {
            if (radius - Constants.MinRadius < Constants.CalculationAccuracy)
            {
                throw new ArgumentException("Неверно указан радиус круга.", nameof(radius));
            }
        }
    }
}