﻿CREATE DATABASE ProductDemo;

USE ProductDemo;

CREATE TABLE Products (
Id INT PRIMARY KEY,
"Name" TEXT
);

CREATE TABLE Categories (
Id INT PRIMARY KEY,
"Name" TEXT
);

INSERT INTO Products
VALUES
(1, 'Product 1'),
(2, 'Product 2'),
(3, 'Productт 2');


INSERT INTO Categories
VALUES
(1, 'Category 1'),
(2, 'Category 2'),
(3, 'Categoryя 3');

CREATE TABLE ProductCategory (
ProductId INT FOREIGN KEY REFERENCES Products(Id),
CategoryId INT FOREIGN KEY REFERENCES Categories(Id),
PRIMARY KEY (ProductId, CategoryId)
);

INSERT INTO ProductCategory
VALUES
(1, 1),
(2, 3),
(3, 3);

SELECT P."Name", C."Name"
FROM Products P
LEFT JOIN ProductCategory PC
ON P.Id = PC.ProductId
LEFT JOIN Categories C
ON PC.CategoryId = C.Id;